using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public Animator Menuanimator;
    public Animator PlayerMenuanimator;
    public Animator Scoreanimator;

    public GameObject player;
    public Rigidbody2D rb;
    public void PlayButton()
    {
        Menuanimator.SetTrigger("Play");
        PlayerMenuanimator.SetTrigger("Play");
        Scoreanimator.SetTrigger("Drop");
    }

    public void QuitButton()
    {
        Application.Quit();
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMenuAnimControler : MonoBehaviour
{
    public GameObject player;
    public PlayerShooting playerShooting;
    public Rigidbody2D playerRB;

    public Animator animator;

    public Vector3 playerStartPosition;
    public void PlayerMenuDisapear()
    {
        gameObject.SetActive(false);
    }

    public void ResetPlayerPosition()
    {
        playerRB.gravityScale = 0;
        player.transform.position = playerStartPosition;
        playerRB.velocity = Vector3.zero;
    }

    public void PlayerDrop()
    {
        player.SetActive(true);
        player.GetComponent<PlayerController>().dead = false;
        playerShooting.canShoot = true;
        playerShooting.spriteRenderer.sprite = playerShooting.idleSprite;
    }

    public void ObstacleStartSpawn()
    {
        LevelManager.instance.blockObstacleSpawn = false;
        LevelManager.instance.blockEnemySpawn = false;
    }

    public void PlayerMenuApear()
    {
        gameObject.SetActive(true);
    }

    public void PlayerMenuRevived()
    {
        gameObject.SetActive(true);
        animator.SetTrigger("Revived");
    }
}

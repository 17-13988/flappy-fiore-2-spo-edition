using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterSelection : MonoBehaviour
{
    public static CharacterSelection instance;

    public GameObject player;
    SpriteRenderer playerSprite;
    public PlayerShooting playerShooting;

    public GameObject menuPlayer;
    SpriteRenderer playerMenuSprite;

    [Header("Fiore")]
    public Sprite Fiore_Idle;
    public Sprite Fiore_Shoot;
    public AudioClip Fiore_Selected;
    public AudioClip Fiore_jumpClip;
    public AudioClip Fiore_shootClip;
    public AudioClip Fiore_deathClip;

    [Header("Spo")]
    public Sprite Spo_Idle;
    public Sprite Spo_Shoot;
    public AudioClip Spo_Selected;
    public AudioClip Spo_jumpClip;
    public AudioClip Spo_shootClip;
    public AudioClip Spo_deathClip;

    [Header("Filo")]
    public Sprite Filo_Idle;
    public Sprite Filo_Shoot;
    public AudioClip Filo_Selected;
    public AudioClip Filo_jumpClip;
    public AudioClip Filo_shootClip;
    public AudioClip Filo_deathClip;

    private void Awake()
    {
        instance = this;
        playerSprite = player.GetComponent<SpriteRenderer>();
        playerMenuSprite = menuPlayer.GetComponent<SpriteRenderer>();

        //Set Default Character
        SetDefaltOnActivation();
    }
    public void SetDefaltOnActivation()
    {
        //Set Menu Sprite
        playerMenuSprite.sprite = Fiore_Idle;
        //Set Game Sprite
        playerSprite.sprite = Fiore_Idle;
        playerShooting.idleSprite = Fiore_Idle;
        playerShooting.shootSprite = Fiore_Shoot;
        //Set Sounds
        SoundManager.SM.jumpClip = Fiore_jumpClip;
        SoundManager.SM.shootClip = Fiore_shootClip;
        SoundManager.SM.deathClip = Fiore_deathClip;
    }

    public void Fiore()
    {
        //Set Menu Sprite
        playerMenuSprite.sprite = Fiore_Idle;
        //Set Game Sprite
        playerSprite.sprite = Fiore_Idle;
        playerShooting.idleSprite = Fiore_Idle;
        playerShooting.shootSprite = Fiore_Shoot;
        //Set Sounds
        SoundManager.SM.jumpClip = Fiore_jumpClip;
        SoundManager.SM.shootClip = Fiore_shootClip;
        SoundManager.SM.deathClip = Fiore_deathClip;
        //Play Selected Character Sound
        SoundManager.SM.sfxSource.PlayOneShot(Fiore_Selected);
    }

    public void Spo()
    {
        //Set Menu Sprite
        playerMenuSprite.sprite = Spo_Idle;
        //Set Game Sprite
        playerSprite.sprite = Spo_Idle;
        playerShooting.idleSprite = Spo_Idle;
        playerShooting.shootSprite = Spo_Shoot;
        //Set Sounds
        SoundManager.SM.jumpClip = Spo_jumpClip;
        SoundManager.SM.shootClip = Spo_shootClip;
        SoundManager.SM.deathClip = Spo_deathClip;
        //Play Selected Character Sound
        SoundManager.SM.sfxSource.PlayOneShot(Spo_Selected);
    }
    public void Filo()
    {
        //Set Menu Sprite
        playerMenuSprite.sprite = Filo_Idle;
        //Set Game Sprite
        playerSprite.sprite = Filo_Idle;
        playerShooting.idleSprite = Filo_Idle;
        playerShooting.shootSprite = Filo_Shoot;
        //Set Sounds
        SoundManager.SM.jumpClip = Filo_jumpClip;
        SoundManager.SM.shootClip = Filo_shootClip;
        SoundManager.SM.deathClip = Filo_deathClip;
        //Play Selected Character Sound
        SoundManager.SM.sfxSource.PlayOneShot(Filo_Selected);
    }
}

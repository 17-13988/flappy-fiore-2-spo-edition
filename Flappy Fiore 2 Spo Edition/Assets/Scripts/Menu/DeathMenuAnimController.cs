using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathMenuAnimController : MonoBehaviour
{
    public Animator animator;

    public void DeathMenuOut()
    {
        animator.SetTrigger("Out");
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingsAnimController : MonoBehaviour
{
    public Animator animator;

    public bool settingsIn;

    public void Pressed()
    {
        if (!settingsIn)
        {
            animator.SetTrigger("SettingsIn");
            settingsIn = !settingsIn;
        }
        else if (settingsIn)
        {
            animator.SetTrigger("SettingsOut");
            settingsIn = !settingsIn;
        }
    }

    public void CheckIfPlay()
    {
        if (settingsIn)
        {
            animator.SetTrigger("SettingsOut");
            settingsIn = !settingsIn;
        }
    }
}

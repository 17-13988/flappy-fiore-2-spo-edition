using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static SoundManager SM;

    [Header("Sources")]
    public AudioSource musicSource;
    public AudioSource sfxSource;

    [Header("Music")]
    public AudioClip musicClip;


    [Header("Player SFX")]
    public AudioClip jumpClip;
    public AudioClip deathClip;
    public AudioClip shootClip;

    private void Start()
    {
        SM = this;
        PlayMusic();
    }
    //MUSIC
    public void PlayMusic()
    {
        if (musicClip != null)
        {
            musicSource.clip = musicClip;
            musicSource.Play();
        }     
    }
    //-------------------------------------

    //SFX
    public void PlayJump()
    {
        if (jumpClip != null)
        {
            sfxSource.pitch = UnityEngine.Random.Range(0.95f,1.05f);
            sfxSource.PlayOneShot(jumpClip);
        }
    }
    //-------------------------------------
    public void PlayDeath()
    {
        if (deathClip != null)
        {
            sfxSource.pitch = UnityEngine.Random.Range(0.95f, 1.05f);
            sfxSource.PlayOneShot(deathClip);
        }
    }
    //-------------------------------------
    public void PlayShoot()
    {
        if (shootClip != null)
        {
            sfxSource.pitch = UnityEngine.Random.Range(0.95f, 1.05f);
            sfxSource.PlayOneShot(shootClip);
        }
    }
    //-------------------------------------
}

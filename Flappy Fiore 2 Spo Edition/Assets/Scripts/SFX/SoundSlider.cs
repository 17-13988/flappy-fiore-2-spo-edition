using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundSlider : MonoBehaviour
{
    [SerializeField] Slider musicSlider;
    [SerializeField] Slider sfxSlider;
    [SerializeField] Slider generalSlider;

    [SerializeField] AudioSource musicSource;
    [SerializeField] AudioSource sfxSource;

    private const string MusicVolumeKey = "musicVolume";
    private const string SFXVolumeKey = "sfxVolume";
    private const string GeneralVolumeKey = "generalVolume";

    void Awake()
    {
        // Carica i valori iniziali dai PlayerPrefs o imposta i valori predefiniti se le chiavi non esistono
        if (!PlayerPrefs.HasKey(MusicVolumeKey))
        {
            PlayerPrefs.SetFloat(MusicVolumeKey, 1);
        }
        if (!PlayerPrefs.HasKey(SFXVolumeKey))
        {
            PlayerPrefs.SetFloat(SFXVolumeKey, 1);
        }
        if (!PlayerPrefs.HasKey(GeneralVolumeKey))
        {
            PlayerPrefs.SetFloat(GeneralVolumeKey, 1);
        }
        else
        {
            Load();
        }
        
    }

    public void ChangeMusicVolume()
    {
        musicSource.volume = musicSlider.value;
        SaveMusicVolume();
    }

    public void ChangeSFXVolume()
    {
        sfxSource.volume = sfxSlider.value;
        SaveSFXVolume();
    }

    public void ChangeGeneralVolume()
    {
        AudioListener.volume = generalSlider.value;
        SaveGeneralVolume();
    }

    private void Load()
    {
        musicSlider.value = PlayerPrefs.GetFloat(MusicVolumeKey);
        sfxSlider.value = PlayerPrefs.GetFloat(SFXVolumeKey);
        generalSlider.value = PlayerPrefs.GetFloat(GeneralVolumeKey);

        musicSource.volume = musicSlider.value;
        sfxSource.volume = sfxSlider.value;
        AudioListener.volume = generalSlider.value;
    }

    private void SaveMusicVolume()
    {
        PlayerPrefs.SetFloat(MusicVolumeKey, musicSlider.value);
    }

    private void SaveSFXVolume()
    {
        PlayerPrefs.SetFloat(SFXVolumeKey, sfxSlider.value);
    }

    private void SaveGeneralVolume()
    {
        PlayerPrefs.SetFloat(GeneralVolumeKey, generalSlider.value);
    }
}


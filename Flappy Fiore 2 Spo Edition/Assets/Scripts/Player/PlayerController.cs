using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [Header("Jump")]
    public Rigidbody2D rb;
    public int jumpForce = 500;
    public bool canJump;
    public float maxAltitude;
    //public float groundHeight = -1.0f;
    [Header("FX")]
    public GameObject explosion0;
    public GameObject explosion1;
    public GameObject explosion2;
    public GameObject explosion3;
    public GameObject explosion4;
    [Header("Menu")]
    public GameObject deathMenu;
    public GameObject deathRetryButton;
    public GameObject deathMenuButton;

    public GameObject settingsMenu;

    public Animator scoreAnimator;
    [Header("Pause")]
    public bool gamePaused;
    public GameObject pauseMenu;
    public PlayerShooting playerShooting;
    [Header("Statistics")]
    public float verticalVelocity;
    public float gravityUp;
    public float gravityDown;
    public bool dead;

    void Update()
    {
        if (Input.GetButtonDown("Jump") && CanJump() & !gamePaused)
        {
            Jump();
        }

        verticalVelocity = rb.velocity.y;
        if (verticalVelocity < 0)
        {
            rb.gravityScale = gravityDown;
            transform.rotation = Quaternion.Euler(0, 0, -3);
        }
        else
        {
            rb.gravityScale = gravityUp;
            transform.rotation = Quaternion.Euler(0, 0, 3);
        }

        Pause();
    }
    bool CanJump()
    {
        if (gameObject.transform.position.y >= maxAltitude)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    void Jump()
    {
        rb.velocity = Vector3.zero;
        rb.AddForce(Vector2.up * jumpForce);
        SoundManager.SM.PlayJump();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Ho aggiunto questa linea di codice V 
        if (collision.gameObject.CompareTag("Ground") || collision.gameObject.CompareTag("Obstacle") || collision.gameObject.CompareTag("Enemy") || collision.gameObject.CompareTag("EnemyBullet"))
        {
            Die();
        }
        /*if (!dead && transform.position.y <= groundHeight)
        {
            Die();
        }*/
    }

    public void Die()
    {
        dead = true;

        SoundManager.SM.PlayDeath();

        SpawnParticles();

        deathMenu.GetComponent<Animator>().SetTrigger("Up");
        deathRetryButton.SetActive(true);
        deathMenuButton.SetActive(true);

        LevelManager.instance.blockObstacleSpawn = true;
        LevelManager.instance.blockEnemySpawn = true;
        LevelManager.instance.DeathScore();

        LM_AnimController.instance.Menu();

        scoreAnimator.SetTrigger("SemiUp");

        gameObject.SetActive(false);
    }

    public void SpawnParticles()
    {
        Instantiate(explosion0, transform.position + (new Vector3(0,0,-1)) , Quaternion.identity);
        Instantiate(explosion1, transform.position + (new Vector3(0,0,-1)) , Quaternion.identity);
        Instantiate(explosion2, transform.position + (new Vector3(0,0,-1)) , Quaternion.identity);
        Instantiate(explosion3, transform.position + (new Vector3(0,0,-1)) , Quaternion.identity);
        Instantiate(explosion4, transform.position + (new Vector3(0,0,-1)) , Quaternion.identity);
    }

    public void Pause()
    {
        if (Input.GetButtonDown("Pause") && !gamePaused)
        {
            gamePaused = true;
            Time.timeScale = 0;
            
            pauseMenu.SetActive(true); 
            settingsMenu.SetActive(true);
            
            playerShooting.enabled = false;
        }
        else if (Input.GetButtonDown("Pause") && gamePaused)
        {
            gamePaused = false;
            Time.timeScale = 1;

            pauseMenu.SetActive(false);
            settingsMenu.SetActive(false);

            playerShooting.enabled = true;
        }
    }

    public void MenuPause()
    {
        if (!gamePaused)
        {
            Time.timeScale = 0;
            pauseMenu.SetActive(true);
            playerShooting.enabled = false;
            gamePaused = true;
        }
        else if (gamePaused)
        {
            Time.timeScale = 1;
            pauseMenu.SetActive(false);
            playerShooting.enabled = true;
            gamePaused = false;
        }
    }
}
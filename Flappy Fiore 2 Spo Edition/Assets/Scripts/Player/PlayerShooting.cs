using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShooting : MonoBehaviour
{
    public GameObject projectilePrefab;
    public float projectileVelocity = 10f;
    public float shotInterval = 1f;
    public float delayStart = 0f;
    public Transform Spawnpoint;

    private float shooterRotation;

    public bool canShoot = true;

    public SpriteRenderer spriteRenderer;
    public Sprite idleSprite;
    public Sprite shootSprite;

    private void Update()
    {
        if (Input.GetButtonDown("Fire1") && canShoot)
        {
            Shoot();
            spriteRenderer.sprite = shootSprite;
        }
    }

    public void Shoot()
    {
        StartCoroutine(ReturnIdle());
        GameObject projectile = Instantiate(projectilePrefab, Spawnpoint.position, Quaternion.Euler(0, 0, shooterRotation));
        Rigidbody2D rb = projectile.GetComponent<Rigidbody2D>();
        rb.velocity = transform.right * projectileVelocity;
        SoundManager.SM.PlayShoot();
    }

    IEnumerator ReturnIdle()
    {
        canShoot = false;
        yield return new WaitForSeconds(0.3f);
        spriteRenderer.sprite = idleSprite;
        canShoot = true;
    }
}

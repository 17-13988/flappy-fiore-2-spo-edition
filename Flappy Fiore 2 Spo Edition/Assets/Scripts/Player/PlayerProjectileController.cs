using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerProjectileController : MonoBehaviour
{
    public float rotationSpeed = 500f;
    public GameObject particles;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Obstacle") || collision.CompareTag("Enemy") || collision.CompareTag("EnemyBullet") || collision.CompareTag("BulletStopper"))
        {
            if (collision.CompareTag("BulletStopper"))
            {
                Destroy(gameObject);
            }
            else
            {
                SpawnParticles();
                Destroy(gameObject);
            }
        }
    }

    void Update()
    {
        RotateProjectile();
    }

    void RotateProjectile()
    {
        transform.Rotate(0, 0, rotationSpeed * Time.deltaTime);
    }

    public void SpawnParticles()
    {
        Instantiate(particles, transform.position + (new Vector3(0, 0, -1)), Quaternion.identity);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyProjectileController : MonoBehaviour
{
    public float rotationSpeed = 500f;
    public GameObject particles;


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Projectile") || collision.gameObject.CompareTag("BulletStopper"))
        {
            SpawnParticles();
            Destroy(gameObject);
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player") || collision.gameObject.CompareTag("Obstacle"))
        {
            SpawnParticles();
            Destroy(gameObject);
        }
    }
  
    void Update()
    {
        RotateProjectile();
    }

    void RotateProjectile() 
        {
        transform.Rotate(0, 0, rotationSpeed * Time.deltaTime);
    }

    public void SpawnParticles()
    {
        Instantiate(particles, transform.position + (new Vector3(0, 0, -1)), Quaternion.identity);
    }
}

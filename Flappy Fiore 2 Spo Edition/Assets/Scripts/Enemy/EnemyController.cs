using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    [Header("Projectile")]
    public GameObject projectilePrefab;
    [Header("Particelle")]
    public GameObject deathParticles;
    public GameObject plusOne;
    public float projectileVelocity = 10f;
    public float shootInterval = 5f;
    public float delayStart = 0f;
    public Transform Spawnpoint;

    private float shooterRotation;

    public SpriteRenderer spriteRenderer;
    public Sprite idleSprite;
    public Sprite shootSprite;

    [Header("Movement")]
    public float speed;

    public GameObject player;
    public PlayerController playerController;

    void Start()
    {
        StartCoroutine(ShootInterval());
        player = GameObject.FindWithTag("Player");
        playerController = player.GetComponent<PlayerController>();
    }

    void Update()
    {
        transform.position += (Vector3.left * speed) * Time.deltaTime;

        if (playerController.dead)
        {
            StartCoroutine(LateDie());
        }
    }

    public void Shoot()
    {
        GameObject projectile = Instantiate(projectilePrefab, Spawnpoint.position, Quaternion.Euler(0, 0, -shooterRotation));
        Rigidbody2D rb = projectile.GetComponent<Rigidbody2D>();
        rb.velocity = -transform.right * projectileVelocity;
        spriteRenderer.sprite = shootSprite;
        StartCoroutine(ReturnIdle());
    }
    IEnumerator ReturnIdle()
    {
        yield return new WaitForSeconds(0.3f);
        spriteRenderer.sprite = idleSprite;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Projectile"))
        {
            Die();
        }
    }
    IEnumerator ShootInterval()
    {
        yield return new WaitForSeconds(shootInterval);
        Shoot();
        StartCoroutine(ShootInterval());
    }

    public void Die()
    {
        SpawnParticles();
        SpawnPlusOne();
        LevelManager.instance.score++;
        LevelManager.instance.ScoreUpdate();
        LevelManager.instance.HighScoreUpdate();
        Destroy(gameObject);
    }

    IEnumerator LateDie()
    {
        yield return new WaitForSeconds(1);
        SpawnParticles();
        Destroy(gameObject);
    }

    public void SpawnParticles()
    {
        Instantiate(deathParticles, transform.position + (new Vector3(0, 0, -1)), Quaternion.identity);
    }

    public void SpawnPlusOne()
    {
        Instantiate(plusOne, transform.position + (new Vector3(0.5f, 0.5f, -1)), Quaternion.identity);
    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleManager : MonoBehaviour
{
    public float speed;
    public float destroyTime = 6f;

    void Update()
    {
        transform.position += (Vector3.left * speed) * Time.deltaTime;
    }

    private void Start()
    {
        StartCoroutine(DestroyObstacle());
    }

    IEnumerator DestroyObstacle() 
    {
        yield return new WaitForSeconds(destroyTime);
        Destroy(gameObject);
    }
}

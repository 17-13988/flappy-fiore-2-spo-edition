using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LM_AnimController : MonoBehaviour
{
    public static LM_AnimController instance;

    public Animator animator;

    private void Start()
    {
        instance = this;
    }

    public void Menu()
    {
        animator.SetTrigger("Menu");
        LevelManager.instance.spawnObstacleRate = 2;
        LevelManager.instance.spawnEnemyRate = 5;
    }
    public void Stage1()
    {
        animator.SetTrigger("Stage1");
        LevelManager.instance.spawnObstacleRate = 2;
        LevelManager.instance.spawnEnemyRate = 5;
    }
    public void Stage2()
    {
        animator.SetTrigger("Stage2");
        LevelManager.instance.spawnObstacleRate = 5;
        LevelManager.instance.spawnEnemyRate = 2;
    }
}

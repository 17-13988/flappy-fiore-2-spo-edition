using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LevelManager : MonoBehaviour
{
    public static LevelManager instance;

    public bool blockObstacleSpawn = true;
    public bool blockEnemySpawn = true;

    [Header("Obstacles")]
    public GameObject obstacle;
    public float spawnObstacleRate;
    private float obstacleTimer;
    public float highPoint;
    public float lowPoint;

    [Header("Enemy")]
    public GameObject enemy;
    public int scoreEnemyThreshold;

    public float spawnEnemyRate;
    private float enemyTimer;
    public float highEnemyPoint;
    public float lowEnemyPoint;

    [Header("Score")]
    public int score;
    public int highScore;
    public TextMeshProUGUI scoreText;
    public TextMeshProUGUI deathScoreText;

    public TextMeshProUGUI highScoreText;
    

    private void Start()
    {
        if (instance == null)
        {
            instance = this;
        }
        else 
        {
            Destroy(gameObject);
        }
        highScore = PlayerPrefs.GetInt("HighScore", 0);
        highScoreText.text = ("HIGHSCORE: " + highScore);
    }

    private void Update()
    {
        ObstacleLogic();
        EnemyLogic();
    }
    void ObstacleLogic()
    {
        if (obstacleTimer < spawnObstacleRate)
        {
            obstacleTimer += Time.deltaTime;
        }
        else if (!blockObstacleSpawn)
        {
            SpawnObstacle();
            obstacleTimer = 0;
        }
    }
    void EnemyLogic()
    {
        if (score > scoreEnemyThreshold)
        {
            if (enemyTimer < spawnEnemyRate)
            {
                enemyTimer += Time.deltaTime;
            }
            else if (!blockEnemySpawn)
            {
                SpawnEnemy();
                enemyTimer = 0;
            }
        }
    }

    void SpawnObstacle()
    {
        Instantiate(obstacle, GetRandomHeight(), Quaternion.identity);
    }

    Vector3 GetRandomHeight()
    {
        return new Vector3 (transform.position.x, Random.Range(highPoint, lowPoint), 0);
    }

    void SpawnEnemy()
    {
        Instantiate(enemy, GetRandomHeight(), Quaternion.identity);
    }

    public void ScoreUpdate()
    {
        scoreText.text = ("SCORE: " + score.ToString());
    }

    public void DeathScore()
    {
        deathScoreText.text = ("SCORE: " + score.ToString());
    }

    public void ResetScore()
    {
        score = 0;
        scoreText.text = ("SCORE: " + score.ToString());
    }

    public void HighScoreUpdate()
    {
        if (score >= highScore)
        {
            highScore = score;
            highScoreText.text = ("HIGHSCORE: " + highScore);
            PlayerPrefs.SetInt("HighScore", highScore);
        }
    }

    public void StopObstacleSpawn()
    {
        blockObstacleSpawn = true;
    }
}